if [ "$#" -ne "1" ]
then
    echo "Broj argumenata nije odgovarajuci"
elif [ ! -d $1 ]
then
    echo "Morate zadati ime direktorijuma"
else
   cd $1
   for f in *
   do
       [ -f $f ] && [ -s $f ] || rm $f
   done
fi 